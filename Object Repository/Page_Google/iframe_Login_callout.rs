<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>iframe_Login_callout</name>
   <tag></tag>
   <elementGuidId>1049b514-61c4-4928-b2e4-2b7e4dc99776</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//iframe[@name='callout']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>iframe[name=&quot;callout&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>iframe</value>
      <webElementGuid>a8d659f4-74b9-4eb2-911d-46d5e3bf3a7d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>presentation</value>
      <webElementGuid>f72bc6ba-8b23-4fa5-99eb-6b09616d59e1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>frameborder</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>7dc81e10-33ab-4683-9b66-ae63dd2b5cce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>scrolling</name>
      <type>Main</type>
      <value>no</value>
      <webElementGuid>e13a87bd-7fd2-469d-a6d9-022e14beb31d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>callout</value>
      <webElementGuid>d3fbfb4e-f180-484e-95b2-b7f453f0f60d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>https://ogs.google.com/widget/callout?prid=19037050&amp;pgid=19037049&amp;puid=38f554ad911e37e4&amp;cce=1&amp;dc=1&amp;origin=https%3A%2F%2Fwww.google.com&amp;cn=callout&amp;pid=1&amp;spid=538&amp;hl=id</value>
      <webElementGuid>b23d8aa0-040a-41a8-925a-e4753cfb4d3b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;gb&quot;)/div[@class=&quot;gb_yd gb_cb gb_nd&quot;]/div[3]/iframe[1]</value>
      <webElementGuid>9dc01056-d9b7-4226-8c1a-ab81d3b7f139</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//iframe[@name='callout']</value>
      <webElementGuid>bb54ece0-3ab7-43f1-8b02-426e09d48927</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='gb']/div/div[3]/iframe</value>
      <webElementGuid>6c945a9a-9518-413e-9aec-30b24f2ecac3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//iframe</value>
      <webElementGuid>bd2904a6-1306-4b3a-846b-b6c4a0727e50</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//iframe[@name = 'callout' and @src = 'https://ogs.google.com/widget/callout?prid=19037050&amp;pgid=19037049&amp;puid=38f554ad911e37e4&amp;cce=1&amp;dc=1&amp;origin=https%3A%2F%2Fwww.google.com&amp;cn=callout&amp;pid=1&amp;spid=538&amp;hl=id']</value>
      <webElementGuid>59f3aef7-47a7-49b2-b7e8-0a2f2cbbd286</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
